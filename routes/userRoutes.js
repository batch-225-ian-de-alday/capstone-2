//dependencies
const express = require("express");
const router = express.Router();
// const auth = require("../auth");
const userController = require("../controllers/userController");

// User registration Routes 
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})



module.exports = router;