// dependencies
const User = require("../models/user");
// const bcrypt = require("bcrypt");
// const Product = require("../models/product");
// const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {

    let newUser = new User ({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        password : reqBody.password,
    })

    return newUser.save().then((user, error) => {
        if (error) {

            return false;

        } else {

            return user;
        };
    });
};