//dependencies
const mongoose = require("mongoose");

// User Schema
const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },

   /*  cart: [
        {
            order: {
                productId: {
                    type: String,
                    required: [true, "Product ID is required"]
                },

                name: {
                    type: String,
                    required: [true, "Product name is required"]
                },

                price: {
                    type: Number,
                    required: [true, "Price is required"]
                }
            },

            TotalPrice : {
                type: Number
            }      
        
        }
    ]    */ 
});


module.exports = mongoose.model("User", userSchema)